using System;

namespace Ripasso_file
{
    class Program
    {
        static void Main(string[] args)
        {
            Gestione_FIle gf = new Gestione_FIle();
            int scelta = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("0.esci\n1.Scrivi su file\n2.Leggi da file\n3.pulisci");
                scelta = Convert.ToInt32(Console.ReadLine());
                switch (scelta)
                {
                    case 0:
                        Console.WriteLine("Arrivederci e lasciate la mancia");
                        Console.ReadKey();
                        break;
                    case 1:
                        gf.Scrivi();
                        Console.ReadKey();
                        break;
                    case 2:
                        gf.Stampa();
                        Console.ReadKey();
                        break;
                    case 3:
                        gf.PulisciT();
                        break;
                    default:
                        Console.WriteLine("valore non valido");
                        break;
                }
            } while (scelta != 0);
        }
    }
}