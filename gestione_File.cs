using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Ripasso_file
{
    class Gestione_FIle
    {
        string Patch = @"C:\Users\miky\Desktop\Informatica\File\file.txt";
        private int[] Lunghezze = { 20, 20, 3 };// 20 nome,cognome 3 età 
    
        public void Scrivi()
        {
            try
            {
                Campi c = new Campi();
                c.Aggiungi();

    
                string record=null;
                // controllo che la lunghezza del file sia ugguale al quella peroposta 
                if (c.Nome.Length > Lunghezze[0])
                {
                    c.Nome=  c.Nome.Remove(Lunghezze[0]);
                }
                if (c.Cognome.Length > Lunghezze[1])
                {
                    c.Cognome = c.Cognome.Remove(Lunghezze[1]);
                }
                if (c.Età.Length > Lunghezze[2])
                {
                    c.Età = c.Età.Remove(Lunghezze[2]);
                }
                // assemblo il record 
                record = c.Nome.PadRight(Lunghezze[0], '.') + c.Cognome.PadRight(Lunghezze[1], '.') + c.Età.PadRight(Lunghezze[2], '.') + System.Environment.NewLine;

                StreamWriter sw = new StreamWriter(Patch, true);
                sw.Write(record);
                sw.Close();
            } catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public void PulisciT()
        {
            try
            {
                StreamWriter sw = new StreamWriter(Patch, false);
                sw.Write(' ');
                sw.Close();

            }catch(IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        private List<Campi> Leggi()
        {
            try
            {
                string a;
                List<Campi> Campii = new List<Campi>();
                string[] campi;
                StreamReader sr = new StreamReader(Patch);
                while ((a = sr.ReadLine()) != null)
                {
                    campi = Ricava_campi(a);
                    Campii.Add(new Campi(campi[0], campi[1], campi[2]));
                }
                sr.Close();
                return Campii;
            }catch(IOException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        private string[] Ricava_campi(string a )
        {
            string[] campi = new string[Lunghezze.Length];
            int pos=0;
            int i=0;
            int j=0;
            do
            {
                campi[j] = a.Substring(pos, Lunghezze[i]);
                campi[j] = campi[j].TrimEnd('.');
                j++;
                pos += Lunghezze[i];
                i++;

            } while (i < Lunghezze.Length);
            return campi;
        }
        public void Stampa()
        {
            List <Campi> Campii = Leggi();
            foreach(Campi c in Campii)
            {
                Console.WriteLine(c);
            }

        }
    }
}